#include "player.h"

// READY TO GRADE
/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
    
    board = new Board();
    turn = side;
    
    //currentMoveScore = 0;
    //bestMoveScore = 0;
    //best_x = 0;
    //best_y = 0;
    
    other = (side == BLACK) ? WHITE : BLACK;

    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
}

/*
 * Destructor for the player.
 */
Player::~Player() {
	delete board;
}
/*
int Player::disc_count(Move *move) {
	Board *test = board->copy();
	test->doMove(move, turn);
	delete test;
	return test->count(turn);
}

int Player::valid_count(Move *move) {
	//int myValid = 0;
	int opValid = 0;
	
	Board *test = board->copy();
	test->doMove(move, turn);
	
	for (int i = 0; i < 8; i ++) {
		for (int j = 0; j < 8; j ++) {
			Move *place = new Move(i, j);
			if (test->checkMove(place, other)) {
				opValid ++;
			}
		}
	}
	return -10*opValid;
}
*/

// Assigns a weight to corners and a negative weight to diagonal squares around corner
// Tested 10 games and won 8/10
int Player::location_count(Move *move) {
	if (move->getX() == 7 || move->getX() == 0) {
		if (move->getY() == 7 || move->getY() == 0) {
			return 6;
		}
	}
	if (move->getX() == 6 || move->getX() == 1) {
		if (move->getY() == 6 || move->getY() == 1) {
			return -1;
		}
	}
	
	
	return 0;
}

Move *Player::minimax(int depth) {
	if (depth == 0) {
		return bestMove();
	}
	
}

Move *Player::bestMove() {
	bool valid = false;
	int best_x = 0;
	int best_y = 0;
	
	int best_score = 0;
	int current_score = 0;
	
	for (int i = 0; i < 8; i ++) {
		for (int j = 0; j < 8; j ++) {
			Move *move = new Move(i, j);
			if (board->checkMove(move, turn)) {
				current_score = location_count(move);
				if (current_score > best_score) {
					valid = true;
					best_score = current_score;
					best_x = i;
					best_y = j;
				}
				board->doMove(move, turn);
				return move;
			}
		}
	 }
	 if (valid) {
		Move *nextMove = new Move(best_x, best_y);
		board->doMove(nextMove, turn);
		return nextMove;
	 }
	 return NULL;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     */ 
     
     /*
     board->doMove(opponentsMove, other);
     std::cerr << board->countBlack() << std::endl;
     for (int i = 0; i < 8; i ++) {
		for (int j = 0; j < 8; j ++) {
			Move *move = new Move(i, j);
			if (board->checkMove(move, turn)) {
				std::cerr << move->getX() << std::endl;
				std::cerr << move->getY() << std::endl;
				board->doMove(move, turn);
				std::cerr << board->countBlack() << std::endl;
				return move;
			}
		}
	 }
	 return NULL;
	 */
	 
	 board->doMove(opponentsMove, other);
	 Move *move = bestMove();
	 return move;

}
