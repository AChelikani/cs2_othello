# READ ME #

I made changes so that my program is a heuristic which takes into account
heavily weighted values for corners because these are power positions. Control
of the corners allows for more tiles to be flipped over as the game progresses
because corners give full access to the edge rows. Furthermore, my heuristic
places negative weighs on the corners next to the corners. This is because if
we occupy the spaces next to the corner, it is very difficult to get the corner
squares.  
