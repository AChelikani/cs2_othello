#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {
private:
	Board *board;
	Side turn;
	Side other;
public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int disc_count(Move *move);
    int valid_count(Move *move);
    int location_count(Move *move); 
    Move *bestMove();
    Move *minimax(int depth);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
};

#endif
